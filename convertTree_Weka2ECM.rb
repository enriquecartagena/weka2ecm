# encoding: utf-8

require 'time'
require 'bigdecimal'
require 'json'
require 'csv'

module Weka
	HUNDRED = BigDecimal.new("100.00")
	#Constants for Weka trees data
	LEVEL_CHAR = "|"
	LEVEL_SET = LEVEL_CHAR + "   "
	CONFIDENCE_CHAR = "/"
	RIGHT_PARENTHESIS_CHAR = ")"
	KEY_LEVEL = "level"
	KEY_IS_LEAF = "is_leaf"
	KEY_PARAMETER = "parameter"
	KEY_OPERATOR = "operator"
	KEY_VALUE = "value"
	KEY_RESULT = "result"
	KEY_CONFIDENCE = "confidence"
	
	#Constants for ECM tree nodes
	NODE_ID = "id"
	NODE_START_NODE = "start_node"
	NODE_CONDITION_PARAMETER = "condition_parameter"
	NODE_CONDITION_OPERATOR = "condition_operator"
	NODE_CONDITION_VALUE = "condition_value"
	NODE_WHEN_TRUE = "when_true"
	NODE_WHEN_FALSE = "when_false"
	NODE_RESULT = "result"
	NODE_CONFIDENCE = "confidence"
	JSON_NODES = "nodes"


	#-----------------------------------------------------------------------	
	
	# Examples of lines:
	#|   |   gps_horizontal_accuracy <= 29.656685
	#|   |   |   gps_course <= 342.773438: indoors (3.0)
	def self.getWekaLineInfo(a_line)
		level = 0
		is_leaf = false
		parameter = nil
		operator = nil
		value = nil
		result =  nil
		confidence = nil
		
		#puts "a_line: <#{a_line}>"
		level = a_line.count(LEVEL_CHAR)  #Number of times the character LEVEL_CHAR appears on the string
		if level == 0
			data_starts = 0
		else
			data_starts = a_line.rindex(LEVEL_CHAR) + LEVEL_SET.length
		end
		
		data = a_line[data_starts, a_line.length - data_starts].split
		parameter = data[0]
		operator = data[1]
		value = data[2]
		if data.length > 3 # is a leaf
			is_leaf = true
			if value[-1] == ':' #For some weird reason sometimes the character ':' comes joined to the value and sometimes after an space character. Why Weka??
				value = value.chomp(':')
				result = data[3]
				confidence = data[4]
			else
				result = data[4]
				confidence = data[5]
			end
		end
		#puts "data: <#{data}>"
		#puts "level: <#{level}>     is_leaf: <#{is_leaf}>    parameter: <#{parameter}>    operator: <#{operator}>    value: <#{value}>    result: <#{result}>    confidence: <#{confidence}>"
		return level, is_leaf, parameter, operator, value, result, confidence
	end
	
	
	#-----------------------------------------------------------------------	
	
	
	#Loads into memory a tree in Weka format
	def self.load2Memory(file_name)
		weka_tree = Array.new
		
		File.foreach(file_name) do |a_line|
			#clean the string and also remove empty lines
			a_line.lstrip! #remove leading whitespaces
			a_line.rstrip! #remove trailing whitespaces
			a_line.chomp!  #remove \n\r characters at the end of the line
			if (a_line != "")  #It's not an empty line
				level, is_leaf, parameter, operator, value, result, confidence = getWekaLineInfo(a_line)
				node = {KEY_LEVEL => level, KEY_IS_LEAF => is_leaf, KEY_PARAMETER => parameter, KEY_OPERATOR => operator, KEY_VALUE => value, KEY_RESULT => result, KEY_CONFIDENCE => confidence}
				weka_tree.push(node)
			end
		end
		
		return weka_tree
	end
		
		
	#-----------------------------------------------------------------------
	
	#Casos
	#	- el arbol actual es una hoja: no llamar a esta funcion con hojas
	#	- 
	
	# Given a weka tree it returns the index of the array storing the last line of its first branch 
	# Do not call this method if the tree is just a leaf
	def self.firstBranchLimitIndex(a_weka_tree)
		target_level = a_weka_tree[0][KEY_LEVEL]
		index = 1
		found = false
		
		while index < a_weka_tree.length and !found
			if (a_weka_tree[index][KEY_LEVEL] == target_level)
				found = true
			else
				index += 1
			end
		end
		return index
	end
	
	
	#-----------------------------------------------------------------------
	# Given some confidence info in weka format it returns confidence in ecm format (percentage)
	# Weka confidence is a string with one or two numbers:
	#   - The first number is the total number of instances (weight of instances) reaching the leaf.
    #   - The second number is the number (weight) of those instances that are misclassified.
	#   - Example of confidence with just one number (4.0)  --> 4 samples reached this leave and all 4 correctly clasified. Confidence: 100%
	#   - Example of confidence with two numbers (8.0/2.0) --> 8 samples reached this leave and 2 were misclassified (6 correctly classified). Confidence: 75%
    #    - (4.0)
	def self.convertConfidence(a_weka_confidence)
		ecm_confidence = "100%"
		
		if (a_weka_confidence.count(CONFIDENCE_CHAR) > 0)  #Number of times the character CONFIDENCE_CHAR appears on the string > 0 => has two numbers
			samples_total = BigDecimal.new(a_weka_confidence[1, a_weka_confidence.rindex(CONFIDENCE_CHAR)-1])
			samples_misclassified = BigDecimal.new(a_weka_confidence[a_weka_confidence.rindex(CONFIDENCE_CHAR)+1, a_weka_confidence.rindex(RIGHT_PARENTHESIS_CHAR) - a_weka_confidence.rindex(CONFIDENCE_CHAR) -1])
			confidence = (((samples_total - samples_misclassified)*HUNDRED)/(samples_total)).round(2, :half_down)
			ecm_confidence = confidence.to_s('F') + "%"
		end
		
		#puts "\t\t\t Inside convertConfidence. Received parameter <#{a_weka_confidence}>  Output confidence: <#{ecm_confidence}>"
		return ecm_confidence
	end
	
	
	#-----------------------------------------------------------------------

		
	def self.convertTree(a_weka_tree, node_id)
		current_weka_node = a_weka_tree[0]
		ecm_tree = Array.new
		ecm_node = Hash.new
		ecm_node[NODE_ID] = node_id
		if node_id == '1'  #It's the start node
			ecm_node[NODE_START_NODE] = true
		end
		
		#puts "current_weka_node: <#{current_weka_node}>"
		#puts "a_weka_tree: #{a_weka_tree}"
		case current_weka_node[KEY_OPERATOR]
		when '<=', '<'
			if current_weka_node[KEY_IS_LEAF]
				ecm_node[NODE_CONDITION_PARAMETER] = current_weka_node[KEY_PARAMETER]
				ecm_node[NODE_CONDITION_OPERATOR] = current_weka_node[KEY_OPERATOR]
				ecm_node[NODE_CONDITION_VALUE] = current_weka_node[KEY_VALUE]
				
				left_node_id = node_id + '.1'
				ecm_node[NODE_WHEN_TRUE] = left_node_id
				left_node = Hash.new
				left_node[NODE_ID] = left_node_id
				left_node[NODE_RESULT] = current_weka_node[KEY_RESULT]
				left_node[NODE_CONFIDENCE] = convertConfidence(current_weka_node[KEY_CONFIDENCE])
				
				right_node_id = node_id + '.2'
				ecm_node[NODE_WHEN_FALSE] = right_node_id
				first_row = 1
				last_row = a_weka_tree.size - 1
				right_branch = convertTree(a_weka_tree[first_row..last_row], right_node_id)
				
				ecm_tree.push(ecm_node)
				ecm_tree.push(left_node)
				ecm_tree += right_branch
				
			else  #current node is not a leaf
				ecm_node[NODE_CONDITION_PARAMETER] = current_weka_node[KEY_PARAMETER]
				ecm_node[NODE_CONDITION_OPERATOR] = current_weka_node[KEY_OPERATOR]
				ecm_node[NODE_CONDITION_VALUE] = current_weka_node[KEY_VALUE]
				
				left_node_id = node_id + '.1'
				ecm_node[NODE_WHEN_TRUE] = left_node_id
				first_row = 1
				last_row = firstBranchLimitIndex(a_weka_tree)-1
				left_branch = convertTree(a_weka_tree[first_row..last_row], left_node_id)
				
				right_node_id = node_id + '.2'
				ecm_node[NODE_WHEN_FALSE] = right_node_id
				first_row = firstBranchLimitIndex (a_weka_tree)
				last_row = a_weka_tree.size - 1
				right_branch = convertTree(a_weka_tree[first_row..last_row], right_node_id)
				
				ecm_tree.push(ecm_node)
				ecm_tree += left_branch + right_branch
			end
		when '>', '>='
			if current_weka_node[KEY_IS_LEAF]
				ecm_node[NODE_RESULT] = current_weka_node[KEY_RESULT]
				ecm_node[NODE_CONFIDENCE] = convertConfidence(current_weka_node[KEY_CONFIDENCE])
				ecm_tree.push(ecm_node)
			else  #current node is not a leaf
				first_row = 1 
				last_row = a_weka_tree.size - 1
				right_branch = convertTree(a_weka_tree[first_row..last_row], node_id) #We keep the node_id
				ecm_tree += right_branch
				
			end
		when '='
			if current_weka_node[KEY_IS_LEAF]
				ecm_node[NODE_CONDITION_PARAMETER] = current_weka_node[KEY_PARAMETER]
				ecm_node[NODE_CONDITION_OPERATOR] = current_weka_node[KEY_OPERATOR]
				ecm_node[NODE_CONDITION_VALUE] = current_weka_node[KEY_VALUE]
				
				left_node_id = node_id + '.1'
				ecm_node[NODE_WHEN_TRUE] = left_node_id
				left_node = Hash.new
				left_node[NODE_ID] = left_node_id
				left_node[NODE_RESULT] = current_weka_node[KEY_RESULT]
				left_node[NODE_CONFIDENCE] = convertConfidence(current_weka_node[KEY_CONFIDENCE])
				
				ecm_tree.push(ecm_node)
				ecm_tree.push(left_node)
				
				if (a_weka_tree.size > 1) #There are more lines below this one
					right_node_id = node_id + '.2'
					ecm_node[NODE_WHEN_FALSE] = right_node_id
					first_row = 1
					last_row = a_weka_tree.size - 1
					right_branch = convertTree(a_weka_tree[first_row..last_row], right_node_id)
					ecm_tree += right_branch
				end
			else  #current node is not a leaf
				ecm_node[NODE_CONDITION_PARAMETER] = current_weka_node[KEY_PARAMETER]
				ecm_node[NODE_CONDITION_OPERATOR] = current_weka_node[KEY_OPERATOR]
				ecm_node[NODE_CONDITION_VALUE] = current_weka_node[KEY_VALUE]
				
				left_node_id = node_id + '.1'
				ecm_node[NODE_WHEN_TRUE] = left_node_id
				first_row = 1
				last_row = firstBranchLimitIndex(a_weka_tree) - 1
				left_branch = convertTree(a_weka_tree[first_row..last_row], left_node_id)
				
				ecm_tree.push(ecm_node)
				ecm_tree += left_branch
				
				if (a_weka_tree.size > last_row+1)
					right_node_id = node_id + '.2'
					ecm_node[NODE_WHEN_FALSE] = right_node_id
					first_row = firstBranchLimitIndex (a_weka_tree)
					last_row = a_weka_tree.size - 1
					right_branch = convertTree(a_weka_tree[first_row..last_row], right_node_id)
					
					ecm_tree += right_branch
				end
			end
		else
			puts "I don't know how to process operator #{current_weka_node[KEY_OPERATOR]}"
		end
		return ecm_tree
	end
		
		
	#-----------------------------------------------------------------------
	#   MAIN PART
	#-----------------------------------------------------------------------
	# quit unless our script gets two command line arguments
	unless ARGV.length == 2
	  puts "Sorry, not the right number of arguments."
	  puts "Usage: ruby convertTree_Weka2ECM.rb your_input_tree.weka your_output_tree.ecm \n"
	  exit
	end
	input_tree_file_name = ARGV[0]
	output_tree_file_name = ARGV[1]
	
	weka_tree = load2Memory(input_tree_file_name)
	ecm_tree = convertTree(weka_tree, "1")
	
	ecm_tree_json =  JSON.pretty_generate({JSON_NODES => ecm_tree})
	File.open(output_tree_file_name, "w") do |f|
		f.write(ecm_tree_json)
	end
end





  
  
